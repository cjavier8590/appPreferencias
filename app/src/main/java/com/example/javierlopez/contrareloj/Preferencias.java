package com.example.javierlopez.contrareloj;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.RadioButton;
import android.widget.Toast;

/**
 * Created by Javier Lopez on 29/07/2017.
 */

public class Preferencias extends AppCompatActivity {

    private RadioButton javaSi, javaNo, androidSi, androidNo, springSi, springNo;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preferencias);

        javaSi = (RadioButton) findViewById(R.id.javaSi);
        javaNo = (RadioButton) findViewById(R.id.javaNo);
        androidSi = (RadioButton) findViewById(R.id.androidSi);
        androidNo = (RadioButton) findViewById(R.id.androidNo);
        springSi = (RadioButton) findViewById(R.id.springSi);
        springNo = (RadioButton) findViewById(R.id.springNo);

    }

    public void enviar(View view){
        String texto = "";
        if(javaSi.isChecked()) texto = texto + " Java";
        if(androidSi.isChecked()) texto = texto + " Android";
        if(springSi.isChecked()) texto = texto + " Spring";

        Toast.makeText(this, texto, Toast.LENGTH_SHORT).show();
    }
}
