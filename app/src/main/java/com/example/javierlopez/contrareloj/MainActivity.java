package com.example.javierlopez.contrareloj;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private EditText password, confirmacion, nombre, apellido;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        password = (EditText) findViewById(R.id.pass);
        confirmacion = (EditText) findViewById(R.id.confirm);
        nombre = (EditText) findViewById(R.id.nombre);
        apellido = (EditText) findViewById(R.id.apellido);
    }

    public void aceptar(View view){
        String pass = password.getText().toString();
        String confirma = confirmacion.getText().toString();

        if(pass.equals(confirma)){
            Intent i = new Intent(this,Preferencias.class);
            startActivity(i);
            Toast.makeText(this, "Bienvenido", Toast.LENGTH_SHORT).show();
        }
        else{
            Toast.makeText(this, "Las contraseñas no coinciden, por favor intenteleo de nuevo", Toast.LENGTH_SHORT).show();
        }
    }

    public void cancelar(View view){
        password.setText("");
        confirmacion.setText("");
        nombre.setText("");
        apellido.setText("");
        Toast.makeText(this, "Cancelando....", Toast.LENGTH_SHORT).show();
    }
}
